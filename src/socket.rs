use tokio::net::{TcpListener, TcpStream, UdpSocket};

pub enum SocketType {
    TcpListener(TcpListener),
    TcpStream(TcpStream),
    Udp(UdpSocket),
}