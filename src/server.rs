use std::collections::HashMap;
use std::net::SocketAddr;
use std::sync::Arc;
use std::time::Duration;

use futures::channel::oneshot;
use futures::channel::oneshot::Sender;
use futures::prelude::*;
use futures::StreamExt;
use rand::{Rng, thread_rng};
use rand::distributions::Alphanumeric;
use tokio::net::{TcpListener, TcpStream, UdpSocket};
use tokio::prelude::*;
use tokio::runtime::Runtime;
use tokio::sync::Mutex;
use log::{info, debug, error, trace};

use crate::error::DrpError;
use crate::message::{MessageReply, MessageRequest, read_deserialize, write_serialize, BridgeType};
use crate::socket::SocketType;

type PeerInfo = (TcpStream, usize);
type WaitHandler = Sender<SocketType>;
type ShutdownHandler = Sender<()>;

struct ServerInner {
    peers: Mutex<HashMap<String, PeerInfo>>,
    wait_queue: Mutex<HashMap<(String, usize), WaitHandler>>,

    shutdowns: Mutex<HashMap<String, ShutdownHandler>>,
}

impl ServerInner {
    fn new() -> Result<Self, DrpError> {
        Ok(ServerInner {
            peers: Mutex::new(HashMap::new()),
            wait_queue: Mutex::new(HashMap::new()),
            shutdowns: Mutex::new(HashMap::new()),
        })
    }

    async fn add_shutdown_handler(&self, id: String, handler: Sender<()>) {
        self.shutdowns.lock().await.insert(id, handler);
    }

    async fn add_peer(&self, id: String, peer: TcpStream) {
        self.peers.lock().await.insert(id, (peer, 0));
    }

    // send NewCon request to client
    async fn request_connection(&self, id: &String, notify: WaitHandler) -> Result<usize, DrpError> {
        let mut peers = self.peers.lock().await;

        let (peer, cid) = peers.get_mut(id).ok_or(DrpError::PeerNotFound(id.to_owned()))?;

        let req = MessageRequest::NewCon { id: id.clone(), cid: *cid };
        write_serialize(peer, req).await?;

        self.wait_queue.lock().await.insert((id.clone(), *cid), notify);

        let ret = Ok(*cid);
        *cid += 1;

        ret
    }

    async fn pop_notify_handle(&self, id: String, cid: usize) -> Option<WaitHandler> {
        let mut queue = self.wait_queue.lock().await;
        queue.remove(&(id, cid))
    }
}

fn generate_id() -> String {
    thread_rng()
        .sample_iter(&Alphanumeric)
        .take(8)
        .collect()
}

pub struct DrpServer {
    addr: SocketAddr,

    inner: Arc<ServerInner>,
}

impl DrpServer {
    pub fn new(addr: SocketAddr) -> Result<Self, crate::error::DrpError> {
        Ok(DrpServer {
            addr,
            inner: Arc::new(
                ServerInner::new()?
            ),
        })
    }

    pub fn run(&mut self) {
        let rt = Runtime::new().unwrap();
        rt.block_on(future::join(self.start_tcp(), self.start_udp()));
    }

    async fn start_tcp(&self) {
        let listener = TcpListener::bind(&self.addr).await.unwrap();
        let mut incoming = listener.incoming();

        while let Some(stream) = incoming.next().await {
            match stream {
                Ok(stream) => {
                    let inner = Arc::clone(&self.inner);

                    tokio::spawn(handle_peer(inner, stream).map(|_| ()));
                }
                Err(_) => { unimplemented!() }
            }
        }
    }

    async fn start_udp(&self) {}
}

/// 处理 client 方面的请求
async fn handle_peer(inner: Arc<ServerInner>, mut stream: TcpStream) -> Result<(), DrpError> {
    let (mut reader, _writer) = stream.split();

    let req: MessageRequest = read_deserialize(&mut reader).await?;

    match req {
        MessageRequest::Register { bridge_type } => {
            let id = generate_id();
            let (socket, port) = alloc_socket(bridge_type).await?;

            debug!("client id: {}, new port on: {}", id, port);

            let proxy = start_proxy(Arc::clone(&inner), socket, id.clone()).boxed();
            let (shutdown_handler, rx) = oneshot::channel();
            tokio::spawn(future::select(proxy, rx).map(|_| ()));

            write_serialize(&mut stream, MessageReply::RegisterReply { id: id.clone(), port }).await?;

            inner.add_shutdown_handler(id.clone(), shutdown_handler).await;
            inner.add_peer(id, stream).await;
        }
        MessageRequest::Bridge { id, cid } => {
            let sender = inner.pop_notify_handle(id, cid).await;

            match sender {
                Some(mut sender) => {
                    sender.send(SocketType::TcpStream(stream));
                }
                None => {}
            }
        }
        _ => { unreachable!() }
    }


    Ok(())
}

async fn alloc_socket(bridge_type: BridgeType) -> Result<(SocketType, u16), DrpError> {
    let new_addr: SocketAddr = "0.0.0.0:0".parse().unwrap();

    match bridge_type {
        BridgeType::Udp => {
            let socket = UdpSocket::bind(new_addr).await?;
            let port = socket.local_addr()?.port();

            Ok((SocketType::Udp(socket), port))
        }
        BridgeType::Tcp => {
            let socket = TcpListener::bind(new_addr).await?;
            let port = socket.local_addr()?.port();

            Ok((SocketType::TcpListener(socket), port))
        }
    }
}

/// 处理 proxy socket 上的外部连接请求
async fn start_proxy(inner: Arc<ServerInner>, socket: SocketType, id: String) -> Result<(), DrpError> {
    match socket {
        SocketType::TcpListener(listener) => {
            start_proxy_tcp(inner, listener, id).await?;
        }
        SocketType::Udp(udp) => { unimplemented!() }
        SocketType::TcpStream(_) => { unreachable!() }
    }
    Ok(())
}

async fn start_proxy_tcp(inner: Arc<ServerInner>, listener: TcpListener, id: String) -> Result<(), DrpError> {
    let mut incoming = listener.incoming();
    while let Some(i_stream) = incoming.next().await {
        let i_stream = i_stream?;
        let inner = Arc::clone(&inner);
        let id = id.clone();

        tokio::spawn(bridge_tcp(inner, i_stream, id).map(|_|()));
    }

    Ok(())
}

async fn bridge_tcp(inner: Arc<ServerInner>, remote: TcpStream, id: String) -> Result<(), DrpError> {
    let (tx, rx) = oneshot::channel();
    let cid = inner.request_connection(&id, tx).await?;

    let local = rx.timeout(Duration::from_secs(30)).await;

    match local {
        Ok(Ok(SocketType::TcpStream(local))) => {
            bridge(inner, remote, local).await;
            // todo: clean state
        }
        Ok(Ok(_)) => { unreachable!() }
        Ok(Err(_)) => {
            // timeout
            let _ = inner.pop_notify_handle(id, cid).await;
        }

        Err(_) => {}
    }

    Ok(())
}

#[inline]
async fn bridge(inner: Arc<ServerInner>, mut lhs: TcpStream, mut rhs: TcpStream) -> Result<(), DrpError> {
    let (mut l_r, mut l_w) = lhs.split();
    let (mut r_r, mut r_w) = rhs.split();

    future::select(l_r.copy(&mut r_w), r_r.copy(&mut l_w)).await;

    Ok(())
}