use failure::Fail;

use std::io;

#[derive(Fail, Debug)]
pub enum DrpError {
    #[fail(display = "unknown")]
    Unknown,

    #[fail(display = "io error: {}", _0)]
    IOError(#[cause] io::Error),

    #[fail(display = "serde error: {}", _0)]
    SerdeError(#[cause] bincode::Error),

    #[fail(display = "peer {} not found", _0)]
    PeerNotFound(String)
}

impl From<io::Error> for DrpError {
    fn from(e: io::Error) -> Self {
        DrpError::IOError(e)
    }
}

impl From<bincode::Error> for DrpError {
    fn from(e: bincode::Error) -> Self {
        DrpError::SerdeError(e)
    }
}