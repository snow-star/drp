use bytes::Bytes;
use futures::{SinkExt, StreamExt};
use serde::{Deserialize, Serialize};
use serde::de::DeserializeOwned;
use tokio::codec::LengthDelimitedCodec;
use tokio::io::{AsyncRead, AsyncWrite};

use crate::error::DrpError;

#[derive(Serialize, Deserialize, Debug)]
pub enum BridgeType {
    Udp,
    Tcp,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum MessageRequest {
    Register { bridge_type: BridgeType },

    NewCon { id: String, cid: usize },

    Bridge { id: String, cid: usize },
}

#[derive(Serialize, Deserialize, Debug)]
pub enum MessageReply {
    RegisterReply { id: String, port: u16 }
}

pub async fn read_deserialize<R: AsyncRead + Unpin, T: DeserializeOwned>(reader: R) -> Result<T, DrpError> {
    let mut framed = LengthDelimitedCodec::builder().new_read(reader);

    let buf = framed.next().await.ok_or(DrpError::Unknown)??;

    bincode::deserialize(&buf).map_err(DrpError::from)
}

pub async fn write_serialize<W: AsyncWrite + Unpin, T: Serialize>(writer: W, obj: T) -> Result<(), DrpError> {
    let mut framed = LengthDelimitedCodec::builder().new_write(writer);

    let buf = bincode::serialize(&obj)?;

    framed.send(Bytes::from(buf)).await.map_err(DrpError::from)
}