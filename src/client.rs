use std::net::SocketAddr;

use tokio::net::TcpStream;
use tokio::prelude::*;
use tokio::runtime::Runtime;

use crate::error::DrpError;
use crate::message::{write_serialize, MessageRequest, read_deserialize, MessageReply, BridgeType};
use log::{info, debug, error};
use tokio::codec::LengthDelimitedCodec;
use futures::prelude::*;
use bytes::BytesMut;

pub struct Client {
    addr: SocketAddr
}

impl Client {
    pub fn create(addr: SocketAddr, local: SocketAddr) -> Result<Self, DrpError> {
        Ok(Client {
            addr
        })
    }

    pub fn start_sync(&self) {
        let rt = Runtime::new().unwrap();

        rt.block_on(self.start().map_err(|_| ()));
    }

    pub async fn start(&self) -> Result<(), DrpError> {
        debug!("connecting to server...");
        let mut server = TcpStream::connect(&self.addr).await?;

        debug!("write register request");
        write_serialize(&mut server, MessageRequest::Register { bridge_type: BridgeType::Tcp }).await?;

        debug!("wait register reply");
        let reply: MessageReply = read_deserialize(&mut server).await?;

        match reply {
            MessageReply::RegisterReply { id, port } => {
                info!("id: {}, port: {}", id, port);

                let (reader, _writer) = server.split();
                let mut framed = LengthDelimitedCodec::builder().new_read(reader);
                while let Some(buff) = framed.next().await {
                    let buff = buff?;

                    tokio::spawn(handle_request(buff, self.addr.clone()).map(|_| ()));
                }
            }
        };

        Ok(())
    }
}


async fn handle_request(buff: BytesMut, server_addr: SocketAddr) -> Result<(), DrpError> {
    let req: MessageRequest = bincode::deserialize(&buff)?;
    match req {
        MessageRequest::NewCon { id, cid } => {
            let mut conn = TcpStream::connect(server_addr).await?;
            write_serialize(&mut conn, MessageRequest::Bridge { id, cid }).await?;

            bridge(conn).await?;
        }
        _ => {
            error!("unknown request")
        }
    }

    Ok(())
}

async fn bridge(mut remote: TcpStream) -> Result<(), DrpError> {
    let mut local = TcpStream::connect("127.0.0.1:8000").await?;

    let (mut l_r, mut l_w) = local.split();
    let (mut r_r, mut r_w) = remote.split();

    debug!("start bridge");
    let _fu = future::select(l_r.copy(&mut r_w), r_r.copy(&mut l_w)).await;
    debug!("remote or local closed");

    Ok(())
}