use drp::DrpServer;

fn main() {
    dotenv::dotenv().ok();
    env_logger::init();

    let mut s = DrpServer::new("0.0.0.0:5000".parse().unwrap()).unwrap();

    s.run();
}