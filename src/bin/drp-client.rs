use drp::{Client, DrpError};
use tokio::runtime::Runtime;

use log::error;

fn main() {
    dotenv::dotenv().ok();
    env_logger::init();

    let addr = "127.0.0.1:5000".parse().unwrap();
    let client = Client::create(addr, addr.clone()).unwrap();

    let rt = Runtime::new().unwrap();

    rt.block_on(client.start());
}